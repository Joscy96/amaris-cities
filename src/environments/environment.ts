import { IAppConfig } from "src/app/model/interfaces/app-config";

export const environment: IAppConfig = {
  production: false,
  menu: [
    {
      title: 'cities',
      routerLink: '/cities',
      icon: 'home'
    }
  ],
  footer: {
    copyright: 'Jhoscy - Amaris Reserver 2022',
    productName: 'Amaris-Cities'
  },
  brand: {
    logoPath: 'console.png',
    logoMinPath: 'console_min.png',
    logoPathDark: 'console_dark.png'
  },
  cities: [
    {
      name: 'London',
      country_code: 'GB'
    },
    {
      name: 'Rome',
      country_code: 'IT'
    },
    {
      name: 'New York',
      country_code: 'US'
    },
    {
      name: 'Bangkok',
      country_code: 'TH'
    },
    {
      name: 'Los Angeles',
      country_code: 'US'
    }
  ],
  openweather: {
    apiKey: '6d9a6354d49a3ac33995961caa409854'
  },
  yelp: {
    apiKey: 'TtER8lhvAcyKkFxiIgdIista5wlysylQ6Jb-q0fxPTGkICUoeE89mtSKHMRdKGK6NTBlcj9Sa2HZ7Pm_eRkNV6aLM7bzPu6C3fqbmPjlcepagCbE0hxfqNWp3WecYnYx'
  }
};

