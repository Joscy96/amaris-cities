import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject, takeUntil } from 'rxjs';
/** Interfaces */
import { OpenCity, OpenWeather } from '@app/model/interfaces/openweather';
/** Services */
import { WeatherService } from './services/weather.service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.less']
})
export class CitiesComponent implements OnInit, OnDestroy {
  private destroy$: Subject<any>;

  constructor(private router: Router, private weatherService: WeatherService) {
    this.destroy$ = new Subject<any>();
  }

  ngOnInit(): void {
    this.weatherService.mockWeatherData()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => console.log(data));
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  public navigate(city: OpenWeather): void {
    const navCity = city.name.replace(/\s/g, '').toLowerCase();
    this.router.navigate([`/cities/${navCity}`], { state: { cityName: city.name, location: navCity,  lat: city.coord.lat, lon: city.coord.lon }})
  }

  public get CitiesData(): Observable<OpenCity[]> {
    return this.weatherService.getCitiesCoordinates().pipe(takeUntil(this.destroy$));
  }

  public get Cities(): Observable<OpenWeather[]> {
    return this.weatherService.mockWeatherData()
  }
}
