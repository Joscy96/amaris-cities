import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { filter, Observable, of } from 'rxjs';
/** Interfaces */
import { YelpBusiness } from '@app/model/interfaces/business';
/** Mock data */
import { YELP_DATA } from '@app/mock/businessData';
/** env */
import { environment } from 'src/environments/environment';
/** base url */
const baseUrl = " https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search?";

@Injectable()
export class YelpService {

  constructor(private httpClient: HttpClient) { }

  public getCityBusinessData(lat: number, lon: number): Observable<any> {
    const url = baseUrl + 'latitude=' + lat + '&' + 'longitude=' + lon;
    return this.httpClient.get<any>(url, { headers: {
      "accept": "application/json",
      "Access-Control-Allow-Origin": "*",
      'Authorization': `Bearer ${environment.yelp.apiKey}`
    }});
  }

  public mockYelpdata(city: string): Observable<YelpBusiness | any> {
    return of(YELP_DATA.find(yelpCity => yelpCity.city === city))
  }
}
