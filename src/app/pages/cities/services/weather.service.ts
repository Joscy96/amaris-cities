import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GEO_DATA } from '@app/mock/geodata';
import { WEATHER_DATA } from '@app/mock/weatherdata';
import { OpenCity } from '@app/model/interfaces/openweather';
import { forkJoin, map, Observable, of, switchMap } from 'rxjs';
/** env */
import { environment } from 'src/environments/environment';
/** open weather base url */
const baseUrl = "https://api.openweathermap.org/data/2.5/weather?";
const geoUrl = "http://api.openweathermap.org/geo/1.0/direct?";

@Injectable()
export class WeatherService {

  constructor(private httpClient: HttpClient) {}

  private getCitiesRequest(): Observable<any>[] {
    return environment.cities.reduce((acc: Observable<any>[], city) => {
      acc.push(this.getCityCoordinates(city.name));
      return acc;
    }, []);
  }

  private getCityCoordinates(city: string): Observable<any> {
    const url = geoUrl + "q=" + city + "&limit=1" + "&appid=" + environment.openweather.apiKey;
    return this.httpClient.get<any>(url);
  }

  public getCitiesCoordinates(): Observable<OpenCity[]> {
    return forkJoin(this.getCitiesRequest()).pipe(map(results => results.reduce((all, city) => all.concat(city), [])))
  }

  public getCityWeatherData(lat: number, lon: number): Observable<any> {
    const url = baseUrl + `lat=${lat}` + '&' + `lon=${lon}` + '&' + 'appid=' + environment.openweather.apiKey;
    return this.httpClient.get<any>(url);
  }

  public mockWeatherData(): Observable<any> {
    return of(GEO_DATA).pipe(map((geo: OpenCity[]) => {
      return geo.reduce((acc: Observable<any>[], openCity) => {
        const cityWeatherData = WEATHER_DATA.find(city => city.name === openCity.name);
        // console.log('OPENCITY', openCity.name, 'WEATHER CITY', cityWeatherData?.name)
        // console.log('CITY WEATHER DATA', cityWeatherData);
        acc.push(of(cityWeatherData))
        return acc;
      }, [])
    }),
    switchMap(cities => forkJoin(cities)))
  }

  public mockWeatherData2(): Observable<any> {
    return of(GEO_DATA).pipe(map((geo: OpenCity[]) => {
      return geo.reduce((acc: any[], city) => {
        acc.push({ name: city.name, req: this.getCityWeatherData(city.lat, city.lon)})
        return acc;
      }, [])
    }),
    switchMap(cities => {
      const reqs: Observable<any>[] = cities.map(city => city.req);
      return forkJoin(reqs)
    })
    )
  }
}
