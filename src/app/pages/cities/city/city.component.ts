import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, Subject, takeUntil } from 'rxjs';
/** Interface */
import { StateBusiness, YelpBusiness } from '@app/model/interfaces/business';
/** Services */
import { YelpService } from '../services/yelp.service';
@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.less']
})
export class CityComponent implements OnInit, OnDestroy {
  private coordinates: StateBusiness = { cityName: '', location: '', lat: 0, lon: 0};
  private destroy$: Subject<any>;

  constructor(private router: Router, private yelpService: YelpService) {
    this.destroy$ = new Subject<any>();
    if (!router.getCurrentNavigation()?.extras.state) {
      this.router.navigate(['/cities']);
    } else {
      this.coordinates = router.getCurrentNavigation()?.extras.state as StateBusiness;
      console.log('Coordinates', this.coordinates)
    }
   }

  ngOnInit(): void {
    // this.yelpService.getCityBusinessData(this.coordinates.lat, this.coordinates.lon)
    // .pipe(takeUntil(this.destroy$))
    // .subscribe(data => console.log(data));
    this.yelpService.mockYelpdata(this.coordinates.location)
    .pipe(takeUntil(this.destroy$))
    .subscribe(data => console.log(data));
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete()
  }

  public businessYelpPage(url: string): void {
    window.location.href = url;
  }

  public get BusinessData(): Observable<YelpBusiness> {
    return this.yelpService.mockYelpdata(this.coordinates.location).pipe(takeUntil(this.destroy$))
  }

  public get Coordinates(): StateBusiness {
    return this.coordinates;
  }
}
