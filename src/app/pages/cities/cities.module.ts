import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CitiesRoutingModule } from './cities-routing.module';
import { CitiesComponent } from './cities.component';
import { CityComponent } from './city/city.component';
/** Providers */
import { WeatherService } from './services/weather.service';
/** NG ZORRO */
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
/** Providers */
import { YelpService } from './services/yelp.service';
import { IconsProviderModule } from '@app/icons-provider.module';

@NgModule({
  declarations: [
    CitiesComponent,
    CityComponent
  ],
  imports: [
    CommonModule,
    CitiesRoutingModule,
    NzCardModule,
    NzTagModule,
    NzTypographyModule,
    /** icons */
    IconsProviderModule,
  ],
  providers: [
    WeatherService,
    YelpService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CitiesModule { }
