import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ThemeService } from '@core/services/ui/theme.service';
import * as StringUtil from '@core/utils/string-utils';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.less']
})
export class TopBarComponent implements OnInit {

  @Input() isSiderCollapsed = true
  @Output() isCollapsed = new EventEmitter<boolean>();

  stringUtil = StringUtil;
  public darkTheme: boolean = false;

  constructor(private themeService: ThemeService) { }

  ngOnInit(): void {
    this.themeService.theme$.subscribe(theme => {
      if (theme === 'dark') this.darkTheme = true;
    });
  }

  ngOnDestroy(): void { }

  public onCollapsed(): void {
    this.isSiderCollapsed = !this.isSiderCollapsed;
    this.isCollapsed.emit(this.isSiderCollapsed);
  }

  public changeTheme(): void {
    this.darkTheme ? this.themeService.sendTheme('dark') : this.themeService.sendTheme('light');
  }
}

