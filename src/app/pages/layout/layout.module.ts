import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { TopBarComponent } from './top-bar/top-bar.component';
/** NG ZORRO */
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { IconsProviderModule } from 'src/app/icons-provider.module';

@NgModule({
  declarations: [
    LayoutComponent,
    TopBarComponent
  ],
  imports: [
    /** core */
    CommonModule,
    FormsModule,
    LayoutRoutingModule,
    /** icons */
    IconsProviderModule,
    /** external */
    NzLayoutModule,
    NzToolTipModule,
    NzGridModule,
    NzMenuModule,
    NzTypographyModule,
    NzSwitchModule,
    NzDividerModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LayoutModule { }
