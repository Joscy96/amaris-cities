import { Component, OnDestroy, OnInit } from '@angular/core';
import { ThemeService } from '@app/core/services/ui/theme.service';
/** env */
import { environment } from 'src/environments/environment';
import pkg from '../../../../package.json';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.less']
})
export class LayoutComponent implements OnInit, OnDestroy {
  public isSiderCollapsed: boolean = true;

  public menu = environment.menu;
  public footer = environment.footer;
  public brand = environment.brand;

  public version = pkg.version;

  public isDark: boolean;
  public logoPath: string;

  constructor(private themeService: ThemeService) {
    this.themeService.theme$.subscribe(theme => {
      console.log(theme);
      this.isDark = theme === 'dark';
      if (this.isDark) {
        this.logoPath = this.brand.logoPathDark;
      } else {
        this.logoPath = this.brand.logoPath;
      }
    });
   }

  ngOnInit(): void {
  }

  onCollapsed(event: boolean) {
    this.isSiderCollapsed = event;
  }

  onMenuClick(routerLink: string) {
  }


  ngOnDestroy(): void {}

}
