import { Component } from '@angular/core';
import { IconService } from './core/services/ui/icon.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'amaris-cities';

  constructor(private iconService: IconService) {}
}
