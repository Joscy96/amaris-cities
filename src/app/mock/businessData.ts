import { YelpBusiness } from "@app/model/interfaces/business";

export const YELP_DATA: YelpBusiness[] = [
	{
		"city": 'london',
		"businesses": [
			{
				"id": "oK_SLmmAVQg3meguh7LrIA",
				"alias": "dishoom-london",
				"name": "Dishoom",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/PwJntXW3zjx_ipn2sFLOhw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/dishoom-london?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1932,
				"categories": [
					{
						"alias": "indpak",
						"title": "Indian"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.512447030091,
					"longitude": -0.1269579217873
				},
				"transactions": [],
				"price": "££",
				"location": {
					"address1": "12 Upper Saint Martin's Lane",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "WC2H 9FB",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"12 Upper Saint Martin's Lane",
						"London WC2H 9FB",
						"United Kingdom"
					]
				},
				"phone": "+442074209320",
				"display_phone": "+44 20 7420 9320",
				"distance": 564.2211579994282
			},
			{
				"id": "ElxBJGffVB-jV1kMCG9PbQ",
				"alias": "the-national-gallery-london-2",
				"name": "The National Gallery",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/Y2mDtEARDc8m4RLXstgorA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/the-national-gallery-london-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 473,
				"categories": [
					{
						"alias": "artmuseums",
						"title": "Art Museums"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.508929,
					"longitude": -0.128302
				},
				"transactions": [],
				"location": {
					"address1": "Trafalgar Square",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "WC2N 5DN",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"Trafalgar Square",
						"London WC2N 5DN",
						"United Kingdom"
					]
				},
				"phone": "+4420077472885",
				"display_phone": "+44 20077472885",
				"distance": 173.5303025679708
			},
			{
				"id": "YRA2VnhFLChIh1ZD31cXjg",
				"alias": "the-british-museum-london",
				"name": "The British Museum",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/TH9r0NXidmIvPXiA9F1_DQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/the-british-museum-london?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1024,
				"categories": [
					{
						"alias": "museums",
						"title": "Museums"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.518298,
					"longitude": -0.126026
				},
				"transactions": [],
				"location": {
					"address1": "Great Russell Street",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "WC1B 3DG",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"Great Russell Street",
						"London WC1B 3DG",
						"United Kingdom"
					]
				},
				"phone": "+442073238299",
				"display_phone": "+44 20 7323 8299",
				"distance": 1218.0033569988245
			},
			{
				"id": "mc1KLbA1Sy4kUTKo0ZEeRQ",
				"alias": "dishoom-london-7",
				"name": "Dishoom",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/9ok9Iflq--Oxs2w3dbcF9g/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/dishoom-london-7?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 573,
				"categories": [
					{
						"alias": "indpak",
						"title": "Indian"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.513087,
					"longitude": -0.139767
				},
				"transactions": [],
				"price": "££",
				"location": {
					"address1": "22 Kingly Street",
					"address2": null,
					"address3": "",
					"city": "London",
					"zip_code": "W1B 5QP",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"22 Kingly Street",
						"London W1B 5QP",
						"United Kingdom"
					]
				},
				"phone": "+442074209322",
				"display_phone": "+44 20 7420 9322",
				"distance": 1041.9970581463015
			},
			{
				"id": "NPAkaFjtmDC8IKC60u5PAw",
				"alias": "coca-cola-london-eye-london",
				"name": "Coca Cola London Eye",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/ITElBJzYm_2QQXkQDJCRyQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/coca-cola-london-eye-london?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1032,
				"categories": [
					{
						"alias": "landmarks",
						"title": "Landmarks & Historical Buildings"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 51.503364,
					"longitude": -0.11957
				},
				"transactions": [],
				"location": {
					"address1": "Riverside Building",
					"address2": "County Hall",
					"address3": "Westminster Bridge Road",
					"city": "London",
					"zip_code": "SE1 7PB",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"Riverside Building",
						"County Hall",
						"Westminster Bridge Road",
						"London SE1 7PB",
						"United Kingdom"
					]
				},
				"phone": "",
				"display_phone": "",
				"distance": 725.1679144124789
			},
			{
				"id": "N3k9B1iF7irRV5BlqcItNQ",
				"alias": "big-ben-london",
				"name": "Big Ben",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/PaYbWmoAEiS_Q8Wziaxr2A/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/big-ben-london?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 452,
				"categories": [
					{
						"alias": "landmarks",
						"title": "Landmarks & Historical Buildings"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.5007803427897,
					"longitude": -0.124601009192702
				},
				"transactions": [],
				"location": {
					"address1": "",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "SW1A 0AA",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"London SW1A 0AA",
						"United Kingdom"
					]
				},
				"phone": "+442072194272",
				"display_phone": "+44 20 7219 4272",
				"distance": 768.6521064873357
			},
			{
				"id": "KGfryNuNH5mqYddLll9QCw",
				"alias": "gordons-wine-bar-london-4",
				"name": "Gordon's Wine Bar",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/bfARR-wEQXTyAWC9TCZwXw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/gordons-wine-bar-london-4?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 414,
				"categories": [
					{
						"alias": "wine_bars",
						"title": "Wine Bars"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.5079411426091,
					"longitude": -0.123381614685059
				},
				"transactions": [],
				"price": "££",
				"location": {
					"address1": "47 Villiers Street",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "WC2N 6NE",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"47 Villiers Street",
						"London WC2N 6NE",
						"United Kingdom"
					]
				},
				"phone": "+442079301408",
				"display_phone": "+44 20 7930 1408",
				"distance": 311.6548835873812
			},
			{
				"id": "P2FLtbOmyCDCq2Q7g8RGZg",
				"alias": "westminster-abbey-london",
				"name": "Westminster Abbey",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/HWUiLjBmgR5JZMoYm5fkkQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/westminster-abbey-london?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 365,
				"categories": [
					{
						"alias": "churches",
						"title": "Churches"
					},
					{
						"alias": "landmarks",
						"title": "Landmarks & Historical Buildings"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.4993203344498,
					"longitude": -0.127374497474177
				},
				"transactions": [],
				"location": {
					"address1": "20 Dean's Yard",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "SW1P 3PA",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"20 Dean's Yard",
						"London SW1P 3PA",
						"United Kingdom"
					]
				},
				"phone": "+442072225152",
				"display_phone": "+44 20 7222 5152",
				"distance": 898.8994178833201
			},
			{
				"id": "0B-ag3J18TatG9H9EQohGg",
				"alias": "hawksmoor-seven-dials-london-4",
				"name": "Hawksmoor Seven Dials",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/yYxu0Q9uw-StO5TXszf8IQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/hawksmoor-seven-dials-london-4?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 356,
				"categories": [
					{
						"alias": "british",
						"title": "British"
					},
					{
						"alias": "steak",
						"title": "Steakhouses"
					},
					{
						"alias": "cocktailbars",
						"title": "Cocktail Bars"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.5134998914001,
					"longitude": -0.12567396291297
				},
				"transactions": [],
				"price": "£££",
				"location": {
					"address1": "11 Langley Street",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "WC2H 9JG",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"11 Langley Street",
						"London WC2H 9JG",
						"United Kingdom"
					]
				},
				"phone": "+442074209390",
				"display_phone": "+44 20 7420 9390",
				"distance": 694.0506309630106
			},
			{
				"id": "vzJaHABdXQ154saZvQHzPg",
				"alias": "flat-iron-covent-garden",
				"name": "Flat Iron",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/xIpl5Fu31piVZm09LylOVQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/flat-iron-covent-garden?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 198,
				"categories": [
					{
						"alias": "steak",
						"title": "Steakhouses"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.5108326561998,
					"longitude": -0.124100585093387
				},
				"transactions": [],
				"price": "££",
				"location": {
					"address1": "17-18 Henrietta Street",
					"address2": "",
					"address3": "",
					"city": "Covent Garden",
					"zip_code": "WC2E 8QH",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"17-18 Henrietta Street",
						"Covent Garden WC2E 8QH",
						"United Kingdom"
					]
				},
				"phone": "+442030194212",
				"display_phone": "+44 20 3019 4212",
				"distance": 459.6064621648662
			},
			{
				"id": "lMVdwKmsxLgqczUXXfgqGg",
				"alias": "punjab-restaurant-london-2",
				"name": "Punjab Restaurant",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/Y_nSrCiozKDuWQlALyWZRg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/punjab-restaurant-london-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 552,
				"categories": [
					{
						"alias": "indpak",
						"title": "Indian"
					},
					{
						"alias": "vegetarian",
						"title": "Vegetarian"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 51.514992,
					"longitude": -0.12596
				},
				"transactions": [],
				"price": "££",
				"location": {
					"address1": "80 Neal Street",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "WC2H 9PA",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"80 Neal Street",
						"London WC2H 9PA",
						"United Kingdom"
					]
				},
				"phone": "+442078369787",
				"display_phone": "+44 20 7836 9787",
				"distance": 853.7402186619047
			},
			{
				"id": "MwD0_P4DGGipWyyaXgIeKw",
				"alias": "churchill-war-rooms-london-2",
				"name": "Churchill War Rooms",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/hBNHoSwrQy90A0qrSwinWw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/churchill-war-rooms-london-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 209,
				"categories": [
					{
						"alias": "landmarks",
						"title": "Landmarks & Historical Buildings"
					},
					{
						"alias": "museums",
						"title": "Museums"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.502259,
					"longitude": -0.129341
				},
				"transactions": [],
				"location": {
					"address1": "Clive Steps",
					"address2": "King Charles Street",
					"address3": "",
					"city": "London",
					"zip_code": "SW1A 2AQ",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"Clive Steps",
						"King Charles Street",
						"London SW1A 2AQ",
						"United Kingdom"
					]
				},
				"phone": "+442079306961",
				"display_phone": "+44 20 7930 6961",
				"distance": 581.5172795456925
			},
			{
				"id": "qhbCUUn1CM27dvTPUwvAUA",
				"alias": "flat-iron-london-2",
				"name": "Flat Iron",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/BawTx5b2LdfMVHfFDJUMzg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/flat-iron-london-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 403,
				"categories": [
					{
						"alias": "steak",
						"title": "Steakhouses"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.51212,
					"longitude": -0.1384008
				},
				"transactions": [],
				"price": "££",
				"location": {
					"address1": "17 Beak Street",
					"address2": "Soho",
					"address3": "",
					"city": "London",
					"zip_code": "W1F 9RW",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"17 Beak Street",
						"Soho",
						"London W1F 9RW",
						"United Kingdom"
					]
				},
				"phone": "",
				"display_phone": "",
				"distance": 903.1692962695064
			},
			{
				"id": "IBV7BsXnv2gX68nWUARhjw",
				"alias": "mother-mash-london",
				"name": "Mother Mash",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/oOiLc_8eHgI2ouwzonR5yg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/mother-mash-london?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 475,
				"categories": [
					{
						"alias": "british",
						"title": "British"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 51.512935,
					"longitude": -0.139414
				},
				"transactions": [],
				"price": "££",
				"location": {
					"address1": "26 Ganton Street",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "W1F 7QZ",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"26 Ganton Street",
						"London W1F 7QZ",
						"United Kingdom"
					]
				},
				"phone": "+442074949644",
				"display_phone": "+44 20 7494 9644",
				"distance": 1008.3225033801718
			},
			{
				"id": "e5jPAqpCcIAsTtasUN5h2g",
				"alias": "the-wolseley-london",
				"name": "The Wolseley",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/T9xpXsz-az88-l0Ko_KLpw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/the-wolseley-london?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 563,
				"categories": [
					{
						"alias": "cafes",
						"title": "Cafes"
					},
					{
						"alias": "british",
						"title": "British"
					},
					{
						"alias": "tea",
						"title": "Tea Rooms"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 51.507617,
					"longitude": -0.141012
				},
				"transactions": [],
				"price": "£££",
				"location": {
					"address1": "160 Picadilly",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "W1J 9EB",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"160 Picadilly",
						"London W1J 9EB",
						"United Kingdom"
					]
				},
				"phone": "+442074996996",
				"display_phone": "+44 20 7499 6996",
				"distance": 914.7074584231452
			},
			{
				"id": "Opa8XUwUghxbGFafvubV6A",
				"alias": "monmouth-coffee-london-3",
				"name": "Monmouth Coffee",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/bwLj-cK2l4ws5hpEefgdqw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/monmouth-coffee-london-3?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 309,
				"categories": [
					{
						"alias": "coffee",
						"title": "Coffee & Tea"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.5143539,
					"longitude": -0.1269168
				},
				"transactions": [],
				"price": "££",
				"location": {
					"address1": "27 Monmouth Street",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "WC2H 9EU",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"27 Monmouth Street",
						"London WC2H 9EU",
						"United Kingdom"
					]
				},
				"phone": "+442072323010",
				"display_phone": "+44 20 7232 3010",
				"distance": 775.6494420637699
			},
			{
				"id": "LC3lv8g8hb8R_VqX1HMO-Q",
				"alias": "st-jamess-park-london-2",
				"name": "St James's Park",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/MrTOL3giCvGp8xuVm5ekNA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/st-jamess-park-london-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 187,
				"categories": [
					{
						"alias": "parks",
						"title": "Parks"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.502552,
					"longitude": -0.134958
				},
				"transactions": [],
				"location": {
					"address1": "",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "SW1A 2BJ",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"London SW1A 2BJ",
						"United Kingdom"
					]
				},
				"phone": "",
				"display_phone": "",
				"distance": 732.1512516731112
			},
			{
				"id": "zdyKTWY6jDasd6Q8BEJdAw",
				"alias": "honest-burgers-meard-st-soho-london",
				"name": "Honest Burgers Meard St - Soho",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/z9i_vprbnt7HobB10Z9rDQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/honest-burgers-meard-st-soho-london?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 283,
				"categories": [
					{
						"alias": "burgers",
						"title": "Burgers"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.5134184063764,
					"longitude": -0.132883885733008
				},
				"transactions": [],
				"price": "££",
				"location": {
					"address1": "4a Meard St",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "W1F 0EF",
					"country": "GB",
					"state": "LND",
					"display_address": [
						"4a Meard St",
						"London W1F 0EF",
						"United Kingdom"
					]
				},
				"phone": "+442036099524",
				"display_phone": "+44 20 3609 9524",
				"distance": 756.0638034262158
			},
			{
				"id": "sYwBQ7mJYhB35nn-_SZstQ",
				"alias": "yauatcha-london-8",
				"name": "Yauatcha",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/Px3lw6UmQLJaZ42MYr7T7g/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/yauatcha-london-8?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 489,
				"categories": [
					{
						"alias": "dimsum",
						"title": "Dim Sum"
					},
					{
						"alias": "seafood",
						"title": "Seafood"
					},
					{
						"alias": "noodles",
						"title": "Noodles"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 51.5137076071076,
					"longitude": -0.135128831605189
				},
				"transactions": [],
				"price": "£££",
				"location": {
					"address1": "15-17 Broadwick Street",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "W1F 0DL",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"15-17 Broadwick Street",
						"London W1F 0DL",
						"United Kingdom"
					]
				},
				"phone": "+442074948888",
				"display_phone": "+44 20 7494 8888",
				"distance": 865.5420491910482
			},
			{
				"id": "LOfW8J-1Tac-7aM04luJrg",
				"alias": "gelupo-london",
				"name": "Gelupo",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/d60JIUrc_y4pmQYQ9ykk1A/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/gelupo-london?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 203,
				"categories": [
					{
						"alias": "icecream",
						"title": "Ice Cream & Frozen Yogurt"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 51.5115509033203,
					"longitude": -0.133706003427505
				},
				"transactions": [],
				"price": "£",
				"location": {
					"address1": "7 Archer Street",
					"address2": "",
					"address3": "",
					"city": "London",
					"zip_code": "W1D 7AU",
					"country": "GB",
					"state": "XGL",
					"display_address": [
						"7 Archer Street",
						"London W1D 7AU",
						"United Kingdom"
					]
				},
				"phone": "+442072875555",
				"display_phone": "+44 20 7287 5555",
				"distance": 616.5204984274203
			}
		],
		"total": 7600,
		"region": {
			"center": {
				"longitude": -0.1278,
				"latitude": 51.5074
			}
		}
	},
	{
		"city": 'rome',
		"businesses": [
			{
				"id": "pIkEKugb8FOl3Qk2kxx5bg",
				"alias": "giolitti-roma-3",
				"name": "Giolitti",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/VQ5M0uzLqQzg7szwHNHuIA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/giolitti-roma-3?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 928,
				"categories": [
					{
						"alias": "icecream",
						"title": "Ice Cream & Frozen Yogurt"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.9010879651567,
					"longitude": 12.4773127734385
				},
				"transactions": [],
				"price": "€",
				"location": {
					"address1": "Via degli Uffici del Vicario 40",
					"address2": null,
					"address3": null,
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via degli Uffici del Vicario 40",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+39066991243",
				"display_phone": "+39 06 699 1243",
				"distance": 880.3335434269756
			},
			{
				"id": "i-r9xiwKBkWhknxSfZww0w",
				"alias": "frigidarium-roma",
				"name": "Frigidarium",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/xxIyTIyc0wfUJ_adKdg5GQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/frigidarium-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 541,
				"categories": [
					{
						"alias": "icecream",
						"title": "Ice Cream & Frozen Yogurt"
					},
					{
						"alias": "desserts",
						"title": "Desserts"
					},
					{
						"alias": "creperies",
						"title": "Creperies"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.8982940871793,
					"longitude": 12.4704499525814
				},
				"transactions": [],
				"price": "€",
				"location": {
					"address1": "Via del Governo Vecchio 112",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via del Governo Vecchio 112",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+393349951184",
				"display_phone": "+39 334 995 1184",
				"distance": 690.100882411174
			},
			{
				"id": "F6dyeWO0Ti0SiUnwfyeBkw",
				"alias": "pantheon-basilica-di-santa-maria-ad-martyres-roma",
				"name": "Pantheon - Basilica di Santa Maria ad Martyres",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/wDcbtQyxePfBYfHHXYiwGw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/pantheon-basilica-di-santa-maria-ad-martyres-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 417,
				"categories": [
					{
						"alias": "churches",
						"title": "Churches"
					},
					{
						"alias": "landmarks",
						"title": "Landmarks & Historical Buildings"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.898614,
					"longitude": 12.476869
				},
				"transactions": [],
				"location": {
					"address1": "Piazza della Rotonda",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Piazza della Rotonda",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+390668300230",
				"display_phone": "+39 06 6830 0230",
				"distance": 603.2690297246537
			},
			{
				"id": "j-giVdvEJFJgdjKQVWDhSw",
				"alias": "cantina-e-cucina-roma",
				"name": "Cantina e Cucina",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/52TMQ191Mze7f22d7yHtmw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/cantina-e-cucina-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 565,
				"categories": [
					{
						"alias": "cafeteria",
						"title": "Cafeteria"
					},
					{
						"alias": "pizza",
						"title": "Pizza"
					},
					{
						"alias": "cocktailbars",
						"title": "Cocktail Bars"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.89801,
					"longitude": 12.47162
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Via del Governo Vecchio 87",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via del Governo Vecchio 87",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+39066892574",
				"display_phone": "+39 06 689 2574",
				"distance": 607.7237848260622
			},
			{
				"id": "wMDtEfXqKTRXZBx1EzDNng",
				"alias": "colosseo-roma",
				"name": "Colosseo",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/RvKAuI_sdx5gXL708_-izg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/colosseo-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1006,
				"categories": [
					{
						"alias": "localflavor",
						"title": "Local Flavor"
					},
					{
						"alias": "landmarks",
						"title": "Landmarks & Historical Buildings"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.8902496828181,
					"longitude": 12.4922484062616
				},
				"transactions": [],
				"location": {
					"address1": "Piazza del Colosseo 1",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00184",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Piazza del Colosseo 1",
						"00184 Rome",
						"Italy"
					]
				},
				"phone": "+3906699841",
				"display_phone": "+39 06 699841",
				"distance": 1435.2667820525098
			},
			{
				"id": "oVoj_A1FExfvI_7UbAdQgQ",
				"alias": "pane-e-salame-roma-2",
				"name": "Pane e Salame",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/eMvedN8Ka5ozav9Pw-dtuw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/pane-e-salame-roma-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 262,
				"categories": [
					{
						"alias": "wine_bars",
						"title": "Wine Bars"
					},
					{
						"alias": "sandwiches",
						"title": "Sandwiches"
					},
					{
						"alias": "roman",
						"title": "Roman"
					}
				],
				"rating": 5,
				"coordinates": {
					"latitude": 41.9006243095738,
					"longitude": 12.4817441403866
				},
				"transactions": [],
				"price": "€",
				"location": {
					"address1": "Via di Santa Maria in Via 19",
					"address2": "",
					"address3": null,
					"city": "Rome",
					"zip_code": "00187",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via di Santa Maria in Via 19",
						"00187 Rome",
						"Italy"
					]
				},
				"phone": "+39066791352",
				"display_phone": "+39 06 679 1352",
				"distance": 969.0234830728551
			},
			{
				"id": "USo4XWhi2NihR19wRbEYGA",
				"alias": "osteria-da-fortunata-roma",
				"name": "Osteria Da Fortunata",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/NJ3WgHJ5Xqq9NxYvFs6pRQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/osteria-da-fortunata-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 776,
				"categories": [
					{
						"alias": "roman",
						"title": "Roman"
					},
					{
						"alias": "trattorie",
						"title": "Trattorie"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 41.8960926932267,
					"longitude": 12.4717008476776
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Via del Pellegrino 11",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via del Pellegrino 11",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+390660667391",
				"display_phone": "+39 06 6066 7391",
				"distance": 436.0931401922403
			},
			{
				"id": "IjUYBpUMJ_iYylwrcsJLiQ",
				"alias": "fontana-di-trevi-roma-3",
				"name": "Trevi Fountain",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/Fg_SI83cs-tILx6W4y7ktg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/fontana-di-trevi-roma-3?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 798,
				"categories": [
					{
						"alias": "landmarks",
						"title": "Landmarks & Historical Buildings"
					},
					{
						"alias": "localflavor",
						"title": "Local Flavor"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.900931,
					"longitude": 12.48331
				},
				"transactions": [],
				"location": {
					"address1": "Piazza di Trevi",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00187",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Piazza di Trevi",
						"00187 Rome",
						"Italy"
					]
				},
				"phone": "",
				"display_phone": "",
				"distance": 1071.7356370517593
			},
			{
				"id": "Gedwawjli9J8xz-HBttP3w",
				"alias": "hostaria-la-botticella-roma",
				"name": "Hostaria La Botticella",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/NTga-6e48nOK7Eang9wDlw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/hostaria-la-botticella-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 390,
				"categories": [
					{
						"alias": "roman",
						"title": "Roman"
					},
					{
						"alias": "trattorie",
						"title": "Trattorie"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.89033,
					"longitude": 12.4680199
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Vicolo del Leopardo 39A",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00153",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Vicolo del Leopardo 39A",
						"00153 Rome",
						"Italy"
					]
				},
				"phone": "+39065814738",
				"display_phone": "+39 06 581 4738",
				"distance": 688.8726786146749
			},
			{
				"id": "MtnKUDm2a9PckD_qnEAE5w",
				"alias": "roscioli-roma-4",
				"name": "Roscioli",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/5xv47fwdpCIH7X7tF8wuoA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/roscioli-roma-4?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 339,
				"categories": [
					{
						"alias": "salumerie",
						"title": "Salumerie"
					},
					{
						"alias": "italian",
						"title": "Italian"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.8941856607235,
					"longitude": 12.4741937220097
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Via dei Giubbonari 21",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via dei Giubbonari 21",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+39066875287",
				"display_phone": "+39 06 687 5287",
				"distance": 140.2413562170461
			},
			{
				"id": "-i5sf6JOXkYY6BhtO2EMMw",
				"alias": "cantina-dei-papi-roma",
				"name": "Cantina dei Papi",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/bxeMpGLXjcdThYmD-pjeGw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/cantina-dei-papi-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 517,
				"categories": [
					{
						"alias": "wine_bars",
						"title": "Wine Bars"
					},
					{
						"alias": "italian",
						"title": "Italian"
					},
					{
						"alias": "sandwiches",
						"title": "Sandwiches"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.9017494369833,
					"longitude": 12.4846919717593
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Via della Panetteria 34A",
					"address2": "",
					"address3": null,
					"city": "Rome",
					"zip_code": "00187",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via della Panetteria 34A",
						"00187 Rome",
						"Italy"
					]
				},
				"phone": "+39066786990",
				"display_phone": "+39 06 678 6990",
				"distance": 1214.1608742311168
			},
			{
				"id": "sUD6ILaFoJkorOwJrfJ8PA",
				"alias": "la-casa-del-tazza-d-oro-roma",
				"name": "La Casa del Tazza d'Oro",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/oQl27GOfNHle9A6cIozdcA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/la-casa-del-tazza-d-oro-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 312,
				"categories": [
					{
						"alias": "coffee",
						"title": "Coffee & Tea"
					},
					{
						"alias": "cafes",
						"title": "Cafes"
					},
					{
						"alias": "coffeeroasteries",
						"title": "Coffee Roasteries"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.8994494630771,
					"longitude": 12.4774190950948
				},
				"transactions": [],
				"price": "€",
				"location": {
					"address1": "Via degli Orfani 84",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via degli Orfani 84",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+39066789792",
				"display_phone": "+39 06 678 9792",
				"distance": 703.9137643026368
			},
			{
				"id": "VPqBAyBNGVZWj5QKyp4Ayg",
				"alias": "dar-poeta-roma",
				"name": "Dar Poeta",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/NsqmNFeA0ekuy2EaIjeAog/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/dar-poeta-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 413,
				"categories": [
					{
						"alias": "pizza",
						"title": "Pizza"
					},
					{
						"alias": "roman",
						"title": "Roman"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.891409,
					"longitude": 12.46846
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Vicolo del Bologna 45",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00153",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Vicolo del Bologna 45",
						"00153 Rome",
						"Italy"
					]
				},
				"phone": "+39065880516",
				"display_phone": "+39 06 588 0516",
				"distance": 611.7219078401007
			},
			{
				"id": "l_eVnablGcsK3JZ2y-vr5Q",
				"alias": "da-enzo-al-29-roma-2",
				"name": "Da Enzo al 29",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/9w-CIR7AABPwlbTKlxOhmw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/da-enzo-al-29-roma-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 345,
				"categories": [
					{
						"alias": "roman",
						"title": "Roman"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.88812,
					"longitude": 12.47774
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Via dei Vascellari 29",
					"address2": null,
					"address3": null,
					"city": "Rome",
					"zip_code": "00153",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via dei Vascellari 29",
						"00153 Rome",
						"Italy"
					]
				},
				"phone": "+39065812260",
				"display_phone": "+39 06 581 2260",
				"distance": 611.5141715085856
			},
			{
				"id": "L-sP_uYcVjdAUf3grDSCRQ",
				"alias": "da-francesco-roma",
				"name": "Da Francesco",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/Sgp9rEBRMDM4lo009CVtYw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/da-francesco-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 530,
				"categories": [
					{
						"alias": "italian",
						"title": "Italian"
					},
					{
						"alias": "pizza",
						"title": "Pizza"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 41.8993198,
					"longitude": 12.4706596
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Piazza del Fico 29",
					"address2": null,
					"address3": null,
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Piazza del Fico 29",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+39066864009",
				"display_phone": "+39 06 686 4009",
				"distance": 789.544800048013
			},
			{
				"id": "QRlrDzxRIjSiR17wefRo6A",
				"alias": "grazia-e-graziella-roma-2",
				"name": "Grazia & Graziella",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/l2meC97_a0iNq6milQYSbw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/grazia-e-graziella-roma-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 329,
				"categories": [
					{
						"alias": "roman",
						"title": "Roman"
					},
					{
						"alias": "trattorie",
						"title": "Trattorie"
					},
					{
						"alias": "pizza",
						"title": "Pizza"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.8897334508832,
					"longitude": 12.4691508201516
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Largo Fumasoni Biondi 5",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00153",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Largo Fumasoni Biondi 5",
						"00153 Rome",
						"Italy"
					]
				},
				"phone": "+39065880398",
				"display_phone": "+39 06 588 0398",
				"distance": 651.8041145701874
			},
			{
				"id": "SXm3gN3LTwfHUyzK3UgNmA",
				"alias": "piazza-navona-roma",
				"name": "Piazza Navona",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/Rd7rRjgqcQbcf0g6kc7o2g/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/piazza-navona-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 233,
				"categories": [
					{
						"alias": "landmarks",
						"title": "Landmarks & Historical Buildings"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.8989477489038,
					"longitude": 12.4731087684723
				},
				"transactions": [],
				"location": {
					"address1": "Piazza Navona",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Piazza Navona",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "",
				"display_phone": "",
				"distance": 656.0095350815573
			},
			{
				"id": "_TZC58WWw6gqR_g7HY3nlA",
				"alias": "la-prosciutteria-trastevere-roma",
				"name": "La Prosciutteria - Trastevere",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/v1weaFnh1blLsPxb-oEeOA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/la-prosciutteria-trastevere-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 132,
				"categories": [
					{
						"alias": "salumerie",
						"title": "Salumerie"
					},
					{
						"alias": "wine_bars",
						"title": "Wine Bars"
					},
					{
						"alias": "sandwiches",
						"title": "Sandwiches"
					}
				],
				"rating": 5,
				"coordinates": {
					"latitude": 41.8906599,
					"longitude": 12.46853
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Via della Scala 71",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00153",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Via della Scala 71",
						"00153 Rome",
						"Italy"
					]
				},
				"phone": "+390664562839",
				"display_phone": "+39 06 6456 2839",
				"distance": 630.6313963187745
			},
			{
				"id": "6mjoD7mVejUHY7eJVCMapg",
				"alias": "sant-eustachio-il-caffè-roma-2",
				"name": "Sant' Eustachio Il Caffè",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/jRpRQjvubK0FKg7p_OhIeQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/sant-eustachio-il-caff%C3%A8-roma-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 335,
				"categories": [
					{
						"alias": "cafes",
						"title": "Cafes"
					},
					{
						"alias": "coffeeroasteries",
						"title": "Coffee Roasteries"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 41.898281,
					"longitude": 12.47543
				},
				"transactions": [],
				"price": "€",
				"location": {
					"address1": "Piazza Sant'Eustachio 81",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Piazza Sant'Eustachio 81",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+390668802048",
				"display_phone": "+39 06 6880 2048",
				"distance": 553.8668813510651
			},
			{
				"id": "CUtvD9xZr-1AtJPUVWKvHQ",
				"alias": "la-vecchia-locanda-roma",
				"name": "La Vecchia Locanda",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/iGRR81JT8txNRVOXt19Gzg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/la-vecchia-locanda-roma?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 171,
				"categories": [
					{
						"alias": "italian",
						"title": "Italian"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 41.8969541,
					"longitude": 12.4757799
				},
				"transactions": [],
				"price": "€€",
				"location": {
					"address1": "Vicolo Sinibaldi 2",
					"address2": "",
					"address3": "",
					"city": "Rome",
					"zip_code": "00186",
					"country": "IT",
					"state": "RM",
					"display_address": [
						"Vicolo Sinibaldi 2",
						"00186 Rome",
						"Italy"
					]
				},
				"phone": "+390668802831",
				"display_phone": "+39 06 6880 2831",
				"distance": 401.1230475102882
			}
		],
		"total": 4500,
		"region": {
			"center": {
				"longitude": 12.4754,
				"latitude": 41.8933
			}
		}
	},
	{
		"city": 'newyork',
		"businesses": [
			{
				"id": "kViIWJFfAfWPpJOwAXBKGA",
				"alias": "national-september-11-memorial-museum-new-york",
				"name": "National September 11 Memorial Museum",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/mDVK4YkasbNWuXVhu5DtjQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/national-september-11-memorial-museum-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1488,
				"categories": [
					{
						"alias": "museums",
						"title": "Museums"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.71144,
					"longitude": -74.01301
				},
				"transactions": [],
				"location": {
					"address1": "180 Greenwich St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10007",
					"country": "US",
					"state": "NY",
					"display_address": [
						"180 Greenwich St",
						"New York, NY 10007"
					]
				},
				"phone": "+12122665211",
				"display_phone": "(212) 266-5211",
				"distance": 633.4963225638414
			},
			{
				"id": "vk7W3_sQwr7eZbRFsXv6rw",
				"alias": "taiyaki-nyc-new-york",
				"name": "Taiyaki NYC",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/F3Vyd-otu36oE8B8M1XXug/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/taiyaki-nyc-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 3124,
				"categories": [
					{
						"alias": "desserts",
						"title": "Desserts"
					},
					{
						"alias": "japanese",
						"title": "Japanese"
					},
					{
						"alias": "icecream",
						"title": "Ice Cream & Frozen Yogurt"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.71789,
					"longitude": -73.9988
				},
				"transactions": [
					"delivery"
				],
				"price": "$",
				"location": {
					"address1": "119 Baxter St",
					"address2": "",
					"address3": null,
					"city": "New York",
					"zip_code": "10013",
					"country": "US",
					"state": "NY",
					"display_address": [
						"119 Baxter St",
						"New York, NY 10013"
					]
				},
				"phone": "+12129662882",
				"display_phone": "(212) 966-2882",
				"distance": 723.2199468498452
			},
			{
				"id": "zj8Lq1T8KIC5zwFief15jg",
				"alias": "prince-street-pizza-new-york-2",
				"name": "Prince Street Pizza",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/ZAukOyv530w4KjOHC5YY1w/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/prince-street-pizza-new-york-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 4340,
				"categories": [
					{
						"alias": "pizza",
						"title": "Pizza"
					},
					{
						"alias": "italian",
						"title": "Italian"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.72308755605564,
					"longitude": -73.99453001177575
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$",
				"location": {
					"address1": "27 Prince St",
					"address2": null,
					"address3": "",
					"city": "New York",
					"zip_code": "10012",
					"country": "US",
					"state": "NY",
					"display_address": [
						"27 Prince St",
						"New York, NY 10012"
					]
				},
				"phone": "+12129664100",
				"display_phone": "(212) 966-4100",
				"distance": 1374.4842409624875
			},
			{
				"id": "o6q3jm-dU5A6nV3r2lBg9A",
				"alias": "chinatown-ice-cream-factory-new-york",
				"name": "Chinatown Ice Cream Factory",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/tmVqoBZ6JR9B3QawJHTYmQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/chinatown-ice-cream-factory-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 2948,
				"categories": [
					{
						"alias": "icecream",
						"title": "Ice Cream & Frozen Yogurt"
					},
					{
						"alias": "desserts",
						"title": "Desserts"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 40.715452,
					"longitude": -73.99818
				},
				"transactions": [
					"delivery"
				],
				"price": "$",
				"location": {
					"address1": "65 Bayard St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10013",
					"country": "US",
					"state": "NY",
					"display_address": [
						"65 Bayard St",
						"New York, NY 10013"
					]
				},
				"phone": "+12126084170",
				"display_phone": "(212) 608-4170",
				"distance": 671.4161657328859
			},
			{
				"id": "0CjK3esfpFcxIopebzjFxA",
				"alias": "joes-shanghai-new-york-2",
				"name": "Joe's Shanghai",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/xM4eGRjk_EfSc1V8MdkRXw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/joes-shanghai-new-york-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 6710,
				"categories": [
					{
						"alias": "shanghainese",
						"title": "Shanghainese"
					},
					{
						"alias": "seafood",
						"title": "Seafood"
					},
					{
						"alias": "noodles",
						"title": "Noodles"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 40.7156608,
					"longitude": -73.9967012
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "46 Bowery St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10013",
					"country": "US",
					"state": "NY",
					"display_address": [
						"46 Bowery St",
						"New York, NY 10013"
					]
				},
				"phone": "+12122338888",
				"display_phone": "(212) 233-8888",
				"distance": 798.191665281078
			},
			{
				"id": "V7lXZKBDzScDeGB8JmnzSA",
				"alias": "katzs-delicatessen-new-york",
				"name": "Katz's Delicatessen",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/7Yn37rOW4VQDI396jPPoyA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/katzs-delicatessen-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 13588,
				"categories": [
					{
						"alias": "delis",
						"title": "Delis"
					},
					{
						"alias": "sandwiches",
						"title": "Sandwiches"
					},
					{
						"alias": "soup",
						"title": "Soup"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 40.722237,
					"longitude": -73.9875259
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "205 E Houston St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10002",
					"country": "US",
					"state": "NY",
					"display_address": [
						"205 E Houston St",
						"New York, NY 10002"
					]
				},
				"phone": "+12122542246",
				"display_phone": "(212) 254-2246",
				"distance": 1789.6912834413606
			},
			{
				"id": "ga6sRtE0l85iftw_5-W84Q",
				"alias": "dominique-ansel-bakery-new-york",
				"name": "Dominique Ansel Bakery",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/6fwWTMjBM-5zCyxoQgG5lA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/dominique-ansel-bakery-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 5020,
				"categories": [
					{
						"alias": "bakeries",
						"title": "Bakeries"
					},
					{
						"alias": "desserts",
						"title": "Desserts"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 40.72516,
					"longitude": -74.00296
				},
				"transactions": [
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "189 Spring St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10012",
					"country": "US",
					"state": "NY",
					"display_address": [
						"189 Spring St",
						"New York, NY 10012"
					]
				},
				"phone": "+12122192773",
				"display_phone": "(212) 219-2773",
				"distance": 1235.5436611597893
			},
			{
				"id": "oDRZt37ROqadlK3-VI0PfQ",
				"alias": "alimama-new-york-3",
				"name": "Alimama",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/YN194f8UNOn1cn5Svu0uSA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/alimama-new-york-3?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1546,
				"categories": [
					{
						"alias": "desserts",
						"title": "Desserts"
					},
					{
						"alias": "donuts",
						"title": "Donuts"
					},
					{
						"alias": "coffee",
						"title": "Coffee & Tea"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.715768,
					"longitude": -73.9992981
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "89A Bayard St",
					"address2": "",
					"address3": null,
					"city": "New York",
					"zip_code": "10013",
					"country": "US",
					"state": "NY",
					"display_address": [
						"89A Bayard St",
						"New York, NY 10013"
					]
				},
				"phone": "+12122357288",
				"display_phone": "(212) 235-7288",
				"distance": 600.9400780175087
			},
			{
				"id": "mvn2XFJfIPNAlvsy-arzkA",
				"alias": "brooklyn-bridge-brooklyn",
				"name": "Brooklyn Bridge",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/U1hvBcbTL2bjxvhWG02nAA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/brooklyn-bridge-brooklyn?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1476,
				"categories": [
					{
						"alias": "landmarks",
						"title": "Landmarks & Historical Buildings"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.7059677364821,
					"longitude": -73.9966657489186
				},
				"transactions": [],
				"location": {
					"address1": "334 Furman St",
					"address2": null,
					"address3": "",
					"city": "Brooklyn",
					"zip_code": "11201",
					"country": "US",
					"state": "NY",
					"display_address": [
						"334 Furman St",
						"Brooklyn, NY 11201"
					]
				},
				"phone": "+17187246434",
				"display_phone": "(718) 724-6434",
				"distance": 1215.4862337313077
			},
			{
				"id": "X8ZS-dgiMIJvhwf9SaDnjw",
				"alias": "wah-fung-no-1-new-york-2",
				"name": "Wah Fung No 1",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/wdPcgBc3S9vjO9CvtLSrwA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/wah-fung-no-1-new-york-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1754,
				"categories": [
					{
						"alias": "hotdogs",
						"title": "Fast Food"
					},
					{
						"alias": "cantonese",
						"title": "Cantonese"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.71735,
					"longitude": -73.99457
				},
				"transactions": [
					"delivery"
				],
				"price": "$",
				"location": {
					"address1": "79 Chrystie St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10002",
					"country": "US",
					"state": "NY",
					"display_address": [
						"79 Chrystie St",
						"New York, NY 10002"
					]
				},
				"phone": "+12129255175",
				"display_phone": "(212) 925-5175",
				"distance": 1022.1668673365072
			},
			{
				"id": "WG639VkTjmK5dzydd1BBJA",
				"alias": "rubirosa-new-york-2",
				"name": "Rubirosa",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/LuSzR8cpVQRofXOT_bMi1A/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/rubirosa-new-york-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 2654,
				"categories": [
					{
						"alias": "italian",
						"title": "Italian"
					},
					{
						"alias": "pizza",
						"title": "Pizza"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.722766,
					"longitude": -73.996233
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "235 Mulberry St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10012",
					"country": "US",
					"state": "NY",
					"display_address": [
						"235 Mulberry St",
						"New York, NY 10012"
					]
				},
				"phone": "+12129650500",
				"display_phone": "(212) 965-0500",
				"distance": 1250.495186192373
			},
			{
				"id": "WIhm0W9197f_rRtDziq5qQ",
				"alias": "lombardis-pizza-new-york-4",
				"name": "Lombardi's Pizza",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/UZ6V_hobp1KpYDPOHNoCKw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/lombardis-pizza-new-york-4?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 6316,
				"categories": [
					{
						"alias": "pizza",
						"title": "Pizza"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 40.7215934960083,
					"longitude": -73.9955956044561
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "32 Spring St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10012",
					"country": "US",
					"state": "NY",
					"display_address": [
						"32 Spring St",
						"New York, NY 10012"
					]
				},
				"phone": "+12129417994",
				"display_phone": "(212) 941-7994",
				"distance": 1194.40501211126
			},
			{
				"id": "MfbVI5reXLIIs8Cc2HwgQw",
				"alias": "breakroom-new-york",
				"name": "Breakroom",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/tT7KRBOH_V2N1P-yLY1SLg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/breakroom-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1644,
				"categories": [
					{
						"alias": "burgers",
						"title": "Burgers"
					},
					{
						"alias": "tacos",
						"title": "Tacos"
					},
					{
						"alias": "hotdog",
						"title": "Hot Dogs"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.7166907225542,
					"longitude": -73.9999440120647
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "83 Baxter St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10013",
					"country": "US",
					"state": "NY",
					"display_address": [
						"83 Baxter St",
						"New York, NY 10013"
					]
				},
				"phone": "+12122272802",
				"display_phone": "(212) 227-2802",
				"distance": 575.4838183818412
			},
			{
				"id": "6G_NSMacAfdf-WYOpcQ4uw",
				"alias": "eileens-special-cheesecake-new-york",
				"name": "Eileen's Special Cheesecake",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/5wemqIS_o0sTwr3JnO7MTg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/eileens-special-cheesecake-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1793,
				"categories": [
					{
						"alias": "bakeries",
						"title": "Bakeries"
					},
					{
						"alias": "desserts",
						"title": "Desserts"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.7215431969029,
					"longitude": -73.9972332037048
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$",
				"location": {
					"address1": "17 Cleveland Pl",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10012",
					"country": "US",
					"state": "NY",
					"display_address": [
						"17 Cleveland Pl",
						"New York, NY 10012"
					]
				},
				"phone": "+12129665585",
				"display_phone": "(212) 966-5585",
				"distance": 1092.9656559927084
			},
			{
				"id": "44SY464xDHbvOcjDzRbKkQ",
				"alias": "ippudo-ny-new-york-7",
				"name": "Ippudo NY",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/zF3EgqHCk7zBUwD2B3WTEA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/ippudo-ny-new-york-7?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 10255,
				"categories": [
					{
						"alias": "ramen",
						"title": "Ramen"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 40.73092,
					"longitude": -73.99015
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "65 4th Ave",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10003",
					"country": "US",
					"state": "NY",
					"display_address": [
						"65 4th Ave",
						"New York, NY 10003"
					]
				},
				"phone": "+12123880088",
				"display_phone": "(212) 388-0088",
				"distance": 2277.545622679338
			},
			{
				"id": "HxC2ZN010NxAFwjTqUVpzw",
				"alias": "rice-to-riches-new-york",
				"name": "Rice To Riches",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/YcJMOVwMfrqTqX1Eo5XFoQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/rice-to-riches-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 3125,
				"categories": [
					{
						"alias": "desserts",
						"title": "Desserts"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 40.721845,
					"longitude": -73.995802
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "37 Spring St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10012",
					"country": "US",
					"state": "NY",
					"display_address": [
						"37 Spring St",
						"New York, NY 10012"
					]
				},
				"phone": "+12122740008",
				"display_phone": "(212) 274-0008",
				"distance": 1201.0581403665863
			},
			{
				"id": "R26zweQ8-xoZS7iHkXQOvw",
				"alias": "shanghai-21-new-york-2",
				"name": "Shanghai 21",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/MGhYHLbutM_8mkf4aYB8XA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/shanghai-21-new-york-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1836,
				"categories": [
					{
						"alias": "shanghainese",
						"title": "Shanghainese"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 40.71441,
					"longitude": -73.99902
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "21 Mott St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10013",
					"country": "US",
					"state": "NY",
					"display_address": [
						"21 Mott St",
						"New York, NY 10013"
					]
				},
				"phone": "+12127666311",
				"display_phone": "(212) 766-6311",
				"distance": 587.3339751853657
			},
			{
				"id": "cGx9CYLxw6fqUQU-MFup8A",
				"alias": "milk-and-cream-cereal-bar-new-york",
				"name": "Milk & Cream Cereal Bar",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/HWrhoU1FzL_lyQeJeNRhBw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/milk-and-cream-cereal-bar-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1093,
				"categories": [
					{
						"alias": "icecream",
						"title": "Ice Cream & Frozen Yogurt"
					},
					{
						"alias": "desserts",
						"title": "Desserts"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.71958,
					"longitude": -73.99654
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "159 Mott St",
					"address2": "",
					"address3": null,
					"city": "New York",
					"zip_code": "10013",
					"country": "US",
					"state": "NY",
					"display_address": [
						"159 Mott St",
						"New York, NY 10013"
					]
				},
				"phone": "",
				"display_phone": "",
				"distance": 991.8033773655644
			},
			{
				"id": "KgpOYAG-r_eDsQXFXt0nnQ",
				"alias": "balthazar-new-york-2",
				"name": "Balthazar",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/CyFAxl4tik3fcrxmIyXGCg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/balthazar-new-york-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 3344,
				"categories": [
					{
						"alias": "french",
						"title": "French"
					},
					{
						"alias": "breakfast_brunch",
						"title": "Breakfast & Brunch"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 40.7226892950401,
					"longitude": -73.9982149184027
				},
				"transactions": [
					"pickup",
					"delivery"
				],
				"price": "$$$",
				"location": {
					"address1": "80 Spring St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10012",
					"country": "US",
					"state": "NY",
					"display_address": [
						"80 Spring St",
						"New York, NY 10012"
					]
				},
				"phone": "+12129651414",
				"display_phone": "(212) 965-1414",
				"distance": 1140.4714692405169
			},
			{
				"id": "fVsH6QIJPzXpVbgmeiolsA",
				"alias": "jungsik-new-york",
				"name": "Jungsik",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/J5lnmvu2eQ64eLHzB6u5iw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/jungsik-new-york?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 696,
				"categories": [
					{
						"alias": "korean",
						"title": "Korean"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 40.7188032533572,
					"longitude": -74.0090829725856
				},
				"transactions": [],
				"price": "$$$$",
				"location": {
					"address1": "2 Harrison St",
					"address2": "",
					"address3": "",
					"city": "New York",
					"zip_code": "10013",
					"country": "US",
					"state": "NY",
					"display_address": [
						"2 Harrison St",
						"New York, NY 10013"
					]
				},
				"phone": "+12122190900",
				"display_phone": "(212) 219-0900",
				"distance": 564.1378413369582
			}
		],
		"total": 6600,
		"region": {
			"center": {
				"longitude": -74.006,
				"latitude": 40.7143
			}
		}
	},
	{
		"city": "nonthaburi",
		"businesses": [],
		"total": 0,
		"region": {
			"center": {
				"longitude": 100.493,
				"latitude": 13.7544
			}
		}
	},
	{
		"city": 'losangeles',
		"businesses": [
			{
				"id": "7O1ORGY36A-2aIENyaJWPg",
				"alias": "howlin-rays-los-angeles-3",
				"name": "Howlin' Ray's",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/MJUkD3lWR3-2hO7fZsg_0g/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/howlin-rays-los-angeles-3?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 7067,
				"categories": [
					{
						"alias": "southern",
						"title": "Southern"
					},
					{
						"alias": "chickenshop",
						"title": "Chicken Shop"
					},
					{
						"alias": "tradamerican",
						"title": "American (Traditional)"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 34.061517,
					"longitude": -118.239716
				},
				"transactions": [
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "727 N Broadway",
					"address2": "Ste 128",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"727 N Broadway",
						"Ste 128",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12139358399",
				"display_phone": "(213) 935-8399",
				"distance": 914.4621610272204
			},
			{
				"id": "iSZpZgVnASwEmlq0DORY2A",
				"alias": "daikokuya-little-tokyo-los-angeles",
				"name": "Daikokuya Little Tokyo",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/GG71SxFbzBd9-SRMRtB1EQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/daikokuya-little-tokyo-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 9020,
				"categories": [
					{
						"alias": "ramen",
						"title": "Ramen"
					},
					{
						"alias": "noodles",
						"title": "Noodles"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 34.05008090944,
					"longitude": -118.2401804513
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$$",
				"location": {
					"address1": "327 E 1st St",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"327 E 1st St",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12136261680",
				"display_phone": "(213) 626-1680",
				"distance": 469.2414347302991
			},
			{
				"id": "TkFEKhsCixPWlShULKvMdQ",
				"alias": "bottega-louie-los-angeles",
				"name": "Bottega Louie",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/rAImnKvUNcNY8i6qEDWrZA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/bottega-louie-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 17228,
				"categories": [
					{
						"alias": "italian",
						"title": "Italian"
					},
					{
						"alias": "desserts",
						"title": "Desserts"
					},
					{
						"alias": "breakfast_brunch",
						"title": "Breakfast & Brunch"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 34.047056,
					"longitude": -118.256544
				},
				"transactions": [
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "700 S Grand Ave",
					"address2": null,
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90017",
					"country": "US",
					"state": "CA",
					"display_address": [
						"700 S Grand Ave",
						"Los Angeles, CA 90017"
					]
				},
				"phone": "+12138021470",
				"display_phone": "(213) 802-1470",
				"distance": 1465.9953557265424
			},
			{
				"id": "fxeuGYnoRWwm5aGDg1FRJA",
				"alias": "marugame-monzo-los-angeles-5",
				"name": "Marugame Monzo",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/ndXsfna0yZGf85Cn94eKzg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/marugame-monzo-los-angeles-5?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 4102,
				"categories": [
					{
						"alias": "japanese",
						"title": "Japanese"
					},
					{
						"alias": "noodles",
						"title": "Noodles"
					},
					{
						"alias": "asianfusion",
						"title": "Asian Fusion"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 34.04994760027338,
					"longitude": -118.24004464723886
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$$",
				"location": {
					"address1": "329 E 1st St",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"329 E 1st St",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12133469762",
				"display_phone": "(213) 346-9762",
				"distance": 488.3986614746237
			},
			{
				"id": "KQBGm5G8IDkE8LeNY45mbA",
				"alias": "wurstküche-los-angeles-2",
				"name": "Wurstküche",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/gWIbvLDR1Y6yfTGTzXp_lw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/wurstk%C3%BCche-los-angeles-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 8689,
				"categories": [
					{
						"alias": "hotdog",
						"title": "Hot Dogs"
					},
					{
						"alias": "german",
						"title": "German"
					},
					{
						"alias": "gastropubs",
						"title": "Gastropubs"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 34.0456047058105,
					"longitude": -118.236061096191
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$$",
				"location": {
					"address1": "800 E 3rd St",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90013",
					"country": "US",
					"state": "CA",
					"display_address": [
						"800 E 3rd St",
						"Los Angeles, CA 90013"
					]
				},
				"phone": "+12136874444",
				"display_phone": "(213) 687-4444",
				"distance": 1093.8197585959163
			},
			{
				"id": "K-Fbh2WCHlqR56H9TnfpNA",
				"alias": "perch-los-angeles",
				"name": "Perch",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/CbKJ-NKAEWmRG-p6WNqn5A/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/perch-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 9021,
				"categories": [
					{
						"alias": "lounges",
						"title": "Lounges"
					},
					{
						"alias": "french",
						"title": "French"
					},
					{
						"alias": "breakfast_brunch",
						"title": "Breakfast & Brunch"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 34.04897,
					"longitude": -118.2514
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$$$",
				"location": {
					"address1": "448 S Hill St",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90013",
					"country": "US",
					"state": "CA",
					"display_address": [
						"448 S Hill St",
						"Los Angeles, CA 90013"
					]
				},
				"phone": "+12138021770",
				"display_phone": "(213) 802-1770",
				"distance": 960.8158013388378
			},
			{
				"id": "b4SH4SbQUJfXxh6hNkF0wg",
				"alias": "eggslut-los-angeles-7",
				"name": "Eggslut",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/MgkGV_YW91JoE5iQBxF7sQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/eggslut-los-angeles-7?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 6075,
				"categories": [
					{
						"alias": "breakfast_brunch",
						"title": "Breakfast & Brunch"
					},
					{
						"alias": "sandwiches",
						"title": "Sandwiches"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 34.05074087639161,
					"longitude": -118.2485842438198
				},
				"transactions": [
					"delivery"
				],
				"price": "$",
				"location": {
					"address1": "317 S Broadway",
					"address2": "",
					"address3": "Grand Central Market",
					"city": "Los Angeles",
					"zip_code": "90013",
					"country": "US",
					"state": "CA",
					"display_address": [
						"317 S Broadway",
						"Grand Central Market",
						"Los Angeles, CA 90013"
					]
				},
				"phone": "+12136250292",
				"display_phone": "(213) 625-0292",
				"distance": 626.289987272639
			},
			{
				"id": "bJVTVJYJL8Es0PzhBsHz8g",
				"alias": "tea-master-matcha-cafe-and-green-tea-shop-los-angeles",
				"name": "Tea Master Matcha Cafe & Green Tea Shop",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/Fua3LsXjQYSdkIXEM9ZYkQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/tea-master-matcha-cafe-and-green-tea-shop-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1643,
				"categories": [
					{
						"alias": "coffee",
						"title": "Coffee & Tea"
					},
					{
						"alias": "icecream",
						"title": "Ice Cream & Frozen Yogurt"
					},
					{
						"alias": "juicebars",
						"title": "Juice Bars & Smoothies"
					}
				],
				"rating": 5,
				"coordinates": {
					"latitude": 34.0471823589909,
					"longitude": -118.238639755523
				},
				"transactions": [
					"delivery"
				],
				"price": "$",
				"location": {
					"address1": "450 E 2nd St",
					"address2": "",
					"address3": null,
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"450 E 2nd St",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12136801006",
				"display_phone": "(213) 680-1006",
				"distance": 831.224627304192
			},
			{
				"id": "YBmk31pBPukmnRyioe5uBA",
				"alias": "sushi-gen-los-angeles",
				"name": "Sushi Gen",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/sAw_hpXedIY_cMzAP5Xa2w/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/sushi-gen-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 3957,
				"categories": [
					{
						"alias": "sushi",
						"title": "Sushi Bars"
					},
					{
						"alias": "japanese",
						"title": "Japanese"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 34.0470000646326,
					"longitude": -118.238502456345
				},
				"transactions": [
					"delivery"
				],
				"price": "$$$",
				"location": {
					"address1": "422 E 2nd St",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"422 E 2nd St",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12136170552",
				"display_phone": "(213) 617-0552",
				"distance": 843.6733302898181
			},
			{
				"id": "hC745S5W4HTq606ySmBJNw",
				"alias": "kazunori-the-original-hand-roll-bar-los-angeles",
				"name": "KazuNori  | The Original Hand Roll Bar",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/9D63gCmIesyBQO15NNG9Xw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/kazunori-the-original-hand-roll-bar-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 3081,
				"categories": [
					{
						"alias": "sushi",
						"title": "Sushi Bars"
					},
					{
						"alias": "japanese",
						"title": "Japanese"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 34.0476390576719,
					"longitude": -118.247747561303
				},
				"transactions": [
					"delivery"
				],
				"price": "$$",
				"location": {
					"address1": "421 S Main St",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90013",
					"country": "US",
					"state": "CA",
					"display_address": [
						"421 S Main St",
						"Los Angeles, CA 90013"
					]
				},
				"phone": "+12134936956",
				"display_phone": "(213) 493-6956",
				"distance": 813.6162816422811
			},
			{
				"id": "gd6PYDx-leOM97a3MWoO1g",
				"alias": "maccheroni-republic-los-angeles",
				"name": "Maccheroni Republic",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/I-4SnHvaWothASzVaMhVYw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/maccheroni-republic-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 3436,
				"categories": [
					{
						"alias": "italian",
						"title": "Italian"
					},
					{
						"alias": "wine_bars",
						"title": "Wine Bars"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 34.050039,
					"longitude": -118.248552
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$$",
				"location": {
					"address1": "332 S Broadway",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90013",
					"country": "US",
					"state": "CA",
					"display_address": [
						"332 S Broadway",
						"Los Angeles, CA 90013"
					]
				},
				"phone": "+12133469725",
				"display_phone": "(213) 346-9725",
				"distance": 668.2328072929922
			},
			{
				"id": "AxPJlSxkCJVJk3sSTL_8OA",
				"alias": "philippe-the-original-los-angeles",
				"name": "Philippe the Original",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/BBNJTUVI3vBp2pLx8aqiIw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/philippe-the-original-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 6604,
				"categories": [
					{
						"alias": "sandwiches",
						"title": "Sandwiches"
					},
					{
						"alias": "breakfast_brunch",
						"title": "Breakfast & Brunch"
					},
					{
						"alias": "tradamerican",
						"title": "American (Traditional)"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 34.0597229003906,
					"longitude": -118.237022399902
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$",
				"location": {
					"address1": "1001 N Alameda St",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"1001 N Alameda St",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12136283781",
				"display_phone": "(213) 628-3781",
				"distance": 855.1438279048759
			},
			{
				"id": "sxN6vf4KNWoK5t1uEdZ4rw",
				"alias": "the-broad-los-angeles-6",
				"name": "The Broad",
				"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/hb99yZMAlQ6hHq8jjc2_Gw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/the-broad-los-angeles-6?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 2458,
				"categories": [
					{
						"alias": "artmuseums",
						"title": "Art Museums"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 34.054493,
					"longitude": -118.250573
				},
				"transactions": [],
				"location": {
					"address1": "221 S Grand Ave",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"221 S Grand Ave",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12132326200",
				"display_phone": "(213) 232-6200",
				"distance": 761.4238139859158
			},
			{
				"id": "8rVWoDNS6JO-5uXOD3VyXw",
				"alias": "cafe-dulce-los-angeles",
				"name": "Cafe Dulce",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/4MmG8pxCQTVE3j5qr8nknw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/cafe-dulce-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 2279,
				"categories": [
					{
						"alias": "coffee",
						"title": "Coffee & Tea"
					},
					{
						"alias": "bakeries",
						"title": "Bakeries"
					},
					{
						"alias": "cafes",
						"title": "Cafes"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 34.04886,
					"longitude": -118.240455
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$",
				"location": {
					"address1": "134 Japanese Village Plz",
					"address2": "Bldg E",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"134 Japanese Village Plz",
						"Bldg E",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12133469910",
				"display_phone": "(213) 346-9910",
				"distance": 579.9268261631643
			},
			{
				"id": "fEY0zHaDMfIW3-N__joDKQ",
				"alias": "bestia-los-angeles",
				"name": "Bestia",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/QWniJCG7Jk0GXf9u8lNI4g/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/bestia-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 6772,
				"categories": [
					{
						"alias": "italian",
						"title": "Italian"
					},
					{
						"alias": "cocktailbars",
						"title": "Cocktail Bars"
					},
					{
						"alias": "pizza",
						"title": "Pizza"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 34.033879426428456,
					"longitude": -118.22930607055224
				},
				"transactions": [
					"delivery"
				],
				"price": "$$$",
				"location": {
					"address1": "2121 E 7th Pl",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90021",
					"country": "US",
					"state": "CA",
					"display_address": [
						"2121 E 7th Pl",
						"Los Angeles, CA 90021"
					]
				},
				"phone": "+12135145724",
				"display_phone": "(213) 514-5724",
				"distance": 2530.444046668519
			},
			{
				"id": "Hg3YYJFGFW7eLWzWZXdYTA",
				"alias": "fugetsu-do-los-angeles",
				"name": "Fugetsu-Do",
				"image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/YD7VeN44wbYkdKpN3k8baA/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/fugetsu-do-los-angeles?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1019,
				"categories": [
					{
						"alias": "candy",
						"title": "Candy Stores"
					},
					{
						"alias": "bakeries",
						"title": "Bakeries"
					},
					{
						"alias": "desserts",
						"title": "Desserts"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 34.05027,
					"longitude": -118.24022
				},
				"transactions": [],
				"price": "$$",
				"location": {
					"address1": "315 E 1st St",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"315 E 1st St",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12136258595",
				"display_phone": "(213) 625-8595",
				"distance": 443.9842550847556
			},
			{
				"id": "0WyRlH-fxOVLh1b3oEBEEQ",
				"alias": "urth-caffé-downtown-la-los-angeles-3",
				"name": "Urth Caffé- Downtown LA",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/iv70AVFqtwZpI3AmMkHZjw/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/urth-caff%C3%A9-downtown-la-los-angeles-3?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 5602,
				"categories": [
					{
						"alias": "coffee",
						"title": "Coffee & Tea"
					},
					{
						"alias": "desserts",
						"title": "Desserts"
					},
					{
						"alias": "sandwiches",
						"title": "Sandwiches"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 34.041974,
					"longitude": -118.235426
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$$",
				"location": {
					"address1": "451 S Hewitt St",
					"address2": "",
					"address3": null,
					"city": "Los Angeles",
					"zip_code": "90013",
					"country": "US",
					"state": "CA",
					"display_address": [
						"451 S Hewitt St",
						"Los Angeles, CA 90013"
					]
				},
				"phone": "+12137974534",
				"display_phone": "(213) 797-4534",
				"distance": 1470.2530229063414
			},
			{
				"id": "CcqraT0cuGKYEcZ1ri_kxg",
				"alias": "broken-mouth-lees-homestyle-los-angeles-5",
				"name": "BROKEN MOUTH | Lee's Homestyle",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/IiLJzHjKd4AlCpS4Ki82kg/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/broken-mouth-lees-homestyle-los-angeles-5?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1211,
				"categories": [
					{
						"alias": "hawaiian",
						"title": "Hawaiian"
					},
					{
						"alias": "korean",
						"title": "Korean"
					},
					{
						"alias": "comfortfood",
						"title": "Comfort Food"
					}
				],
				"rating": 5,
				"coordinates": {
					"latitude": 34.04275,
					"longitude": -118.25081
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$$",
				"location": {
					"address1": "718 S Los Angeles St",
					"address2": null,
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90014",
					"country": "US",
					"state": "CA",
					"display_address": [
						"718 S Los Angeles St",
						"Los Angeles, CA 90014"
					]
				},
				"phone": "+12134189588",
				"display_phone": "(213) 418-9588",
				"distance": 1437.4663051816763
			},
			{
				"id": "tJsZbLxMiykQCek_-diOvg",
				"alias": "sushi-enya-los-angeles-los-angeles-2",
				"name": "Sushi Enya - Los Angeles",
				"image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/szVM1DLajMzg48Lqn_F45A/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/sushi-enya-los-angeles-los-angeles-2?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 1531,
				"categories": [
					{
						"alias": "japanese",
						"title": "Japanese"
					},
					{
						"alias": "sushi",
						"title": "Sushi Bars"
					}
				],
				"rating": 4.5,
				"coordinates": {
					"latitude": 34.049757,
					"longitude": -118.239601
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$$$",
				"location": {
					"address1": "343 E 1st St",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"343 E 1st St",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12133990338",
				"display_phone": "(213) 399-0338",
				"distance": 526.2461772460625
			},
			{
				"id": "jqS3dVaCYeBehb3tOiWWbQ",
				"alias": "shin-sen-gumi-hakata-ramen-little-tokyo-los-angeles-4",
				"name": "Shin-Sen-Gumi Hakata Ramen - Little Tokyo",
				"image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/COFv1hp5eS2J5PSdmMLMCQ/o.jpg",
				"is_closed": false,
				"url": "https://www.yelp.com/biz/shin-sen-gumi-hakata-ramen-little-tokyo-los-angeles-4?adjust_creative=0B6ZN21CZEC0vlYo73zHiw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0B6ZN21CZEC0vlYo73zHiw",
				"review_count": 2937,
				"categories": [
					{
						"alias": "ramen",
						"title": "Ramen"
					},
					{
						"alias": "japacurry",
						"title": "Japanese Curry"
					},
					{
						"alias": "tapasmallplates",
						"title": "Tapas/Small Plates"
					}
				],
				"rating": 4,
				"coordinates": {
					"latitude": 34.048285,
					"longitude": -118.23895
				},
				"transactions": [
					"delivery",
					"pickup"
				],
				"price": "$$",
				"location": {
					"address1": "132 S Central Ave",
					"address2": "",
					"address3": "",
					"city": "Los Angeles",
					"zip_code": "90012",
					"country": "US",
					"state": "CA",
					"display_address": [
						"132 S Central Ave",
						"Los Angeles, CA 90012"
					]
				},
				"phone": "+12136877108",
				"display_phone": "(213) 687-7108",
				"distance": 695.0586904152593
			}
		],
		"total": 2500,
		"region": {
			"center": {
				"longitude": -118.2428,
				"latitude": 34.0537
			}
		}
	}
]