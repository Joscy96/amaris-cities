import { OpenWeather } from "@app/model/interfaces/openweather";

export const WEATHER_DATA: OpenWeather[] = [
    {
      "coord": {
        "lon": -0.1278,
        "lat": 51.5074
      },
      "weather": [
        {
          "id": 801,
          "main": "Clouds",
          "description": "few clouds",
          "icon": "02d"
        }
      ],
      "base": "stations",
      "main": {
        "temp": 286.92,
        "feels_like": 286.36,
        "temp_min": 285.64,
        "temp_max": 288.3,
        "pressure": 1021,
        "humidity": 77
      },
      "visibility": 10000,
      "wind": {
        "speed": 7.2,
        "deg": 60
      },
      "clouds": {
        "all": 20
      },
      "dt": 1654334902,
      "sys": {
        "type": 2,
        "id": 268730,
        "country": "GB",
        "sunrise": 1654314426,
        "sunset": 1654373451
      },
      "timezone": 3600,
      "id": 2643743,
      "name": "London",
      "cod": 200
    },
    {
      "coord": {
        "lon": 12.4754,
        "lat": 41.8933
      },
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "base": "stations",
      "main": {
        "temp": 304.59,
        "feels_like": 303.68,
        "temp_min": 301.74,
        "temp_max": 307.8,
        "pressure": 1017,
        "humidity": 33
      },
      "visibility": 10000,
      "wind": {
        "speed": 3.09,
        "deg": 260
      },
      "clouds": {
        "all": 0
      },
      "dt": 1654335128,
      "sys": {
        "type": 2,
        "id": 2000926,
        "country": "IT",
        "sunrise": 1654313786,
        "sunset": 1654368040
      },
      "timezone": 7200,
      "id": 3169070,
      "name": "Rome",
      "cod": 200
    },
    {
      "coord": {
        "lon": -74.006,
        "lat": 40.7143
      },
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "base": "stations",
      "main": {
        "temp": 289.46,
        "feels_like": 288.97,
        "temp_min": 283.99,
        "temp_max": 292.01,
        "pressure": 1011,
        "humidity": 70
      },
      "visibility": 10000,
      "wind": {
        "speed": 1.54,
        "deg": 260
      },
      "clouds": {
        "all": 0
      },
      "dt": 1654335004,
      "sys": {
        "type": 2,
        "id": 2039034,
        "country": "US",
        "sunrise": 1654334766,
        "sunset": 1654388576
      },
      "timezone": -14400,
      "id": 5128581,
      "name": "New York",
      "cod": 200
    },
    {
      "coord": {
        "lon": 100.493,
        "lat": 13.7544
      },
      "weather": [
        {
          "id": 804,
          "main": "Clouds",
          "description": "overcast clouds",
          "icon": "04d"
        }
      ],
      "base": "stations",
      "main": {
        "temp": 304.05,
        "feels_like": 310.19,
        "temp_min": 303.68,
        "temp_max": 306.45,
        "pressure": 1005,
        "humidity": 69,
        "sea_level": 1005,
        "grnd_level": 1004
      },
      "visibility": 4808,
      "wind": {
        "speed": 5.38,
        "deg": 182,
        "gust": 6.5
      },
      "clouds": {
        "all": 86
      },
      "dt": 1654335652,
      "sys": {
        "type": 2,
        "id": 2032756,
        "country": "TH",
        "sunrise": 1654296574,
        "sunset": 1654342999
      },
      "timezone": 25200,
      "id": 1608132,
      "name": "Nonthaburi",
      "cod": 200
    },
    {
      "coord": {
        "lon": -118.2428,
        "lat": 34.0537
      },
      "weather": [
        {
          "id": 804,
          "main": "Clouds",
          "description": "overcast clouds",
          "icon": "04n"
        }
      ],
      "base": "stations",
      "main": {
        "temp": 290.08,
        "feels_like": 290.02,
        "temp_min": 288.42,
        "temp_max": 291.49,
        "pressure": 1012,
        "humidity": 84
      },
      "visibility": 10000,
      "wind": {
        "speed": 1.54,
        "deg": 0
      },
      "clouds": {
        "all": 100
      },
      "dt": 1654335652,
      "sys": {
        "type": 2,
        "id": 2009067,
        "country": "US",
        "sunrise": 1654346522,
        "sunset": 1654398056
      },
      "timezone": -25200,
      "id": 5368361,
      "name": "Los Angeles",
      "cod": 200
    }
  ]