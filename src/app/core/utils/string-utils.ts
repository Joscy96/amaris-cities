/**
  * Check if a value is null, undefined or empty
  * @param value
  * @returns true if value is null, undefined or empty, otherwise false
  */
 export function isNullUndefinedOrEmpty(value: any): boolean {
    let result: boolean = value === null || value === undefined || value === "" || ((Array.isArray(value) && value.length == 0));
    return result;
  }