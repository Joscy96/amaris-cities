import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject } from 'rxjs';
import * as StringUtil from '@core/utils/string-utils';

const _themeCookieName: string = '_theme';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private themeSource = new BehaviorSubject<string>('');
  public theme$ = this.themeSource.asObservable();

  constructor(private cookieService: CookieService) {
    let _themeCookieValue = this.getThemeCookieValue();
    if (!StringUtil.isNullUndefinedOrEmpty(_themeCookieValue))
      this.sendTheme(_themeCookieValue);
    else
      this.sendTheme('light');
  }

  public sendTheme(theme: string): void {
    if (theme == 'dark') {
      const style = document.createElement('link');
      style.type = 'text/css';
      style.rel = 'stylesheet';
      style.id = 'dark-theme';
      style.href = 'assets/themes/dark.css';
      document.getElementsByTagName('body')[0].appendChild(style);
    } else {
      const dom = document.getElementById('dark-theme');
      if (dom) dom.remove();
    }
    this.themeSource.next(theme);
    this.setThemeCookieValue(theme);
  }

  private getThemeCookieValue(): string {
    return this.cookieService.get(_themeCookieName);
  }

  private setThemeCookieValue(value: string): void {
    this.cookieService.set(_themeCookieName, value);
  }

}
