export interface IAppConfig {
    production: boolean;
    menu: IMenu[];
    footer: IFooter;
    brand: IAppConfigBrand;
    openweather: IOpenWheater;
    yelp: IYelp;
    cities: ICity[];
}

export interface IMenu {
    title: string;
    icon?: string;
    selected?: boolean;
    disabled?: boolean;
    routerLink: string;
    children?: IMenu[];
}

export interface IFooter {
    productName: string;
    copyright: string;
}

export interface IAppConfigBrand {
    logoMinPath: string;
    logoPath: string;
    logoPathDark: string;
}

export interface ICity {
    name: string;
    country_code: string;
}

export interface IOpenWheater {
    apiKey: string;
} 

export interface IYelp {
    apiKey: string;
}
